<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use \Bitrix\Main\Application;

use Bitrix\Main\Loader;

class ExampleCompSimple extends CBitrixComponent
{
    public  $arItem = [];
    private $_request;

    /**
     * Проверка наличия модулей, требуемых для работы компонента
     * @throws Exception
     * @return bool
     */
    private function _checkModules()
    {
        if (!Loader::includeModule('iblock')) {
            throw new \Exception('Не загружены модули необходимые для работы модуля');
        }
        return true;
    }

    /**
     * Получение элементов инфоблока
     *
     * @param $arParams
     *
     * @return string
     */
    public function getElements()
    {
    
        \Bitrix\Main\Loader::includeModule("iblock");
    
        $result = Bitrix\Iblock\ElementTable::getList([
            'select'  => [
                'ID',
                'NAME',
                'PREVIEW_TEXT' => 'PREVIEW_TEXT',
                'IBLOCK.NAME'
            ],
            'filter'  => [
                'IBLOCK_ID' => $this->arParams["IBLOCK_ID"],
                'ACTIVE' => 'Y'
            ]
        ]);
        
    
        while ($arItems = $result->fetch()) {
            $arResult["ITEMS"][] = $arItems;
            $arResult["IBLOCK_ELEMENT_IBLOCK_NAME"] = $arItems["IBLOCK_ELEMENT_IBLOCK_NAME"];
        }
    
        $this->arResult = $arResult;
    }



    /**
     * Запуск компонента
     * @throws \Bitrix\Main\SystemException
     * @return mixed|void
     */
    public function executeComponent()
    {
        $this->_checkModules();
    
        $this->_request = Application::getInstance()->getContext()->getRequest();
    
        $this->getElements();
    
        $this->includeComponentTemplate();
    }
}