jQuery(document).ready(function($) {
    const collapse = $('.faq__collapse'),
          collapseRow = $('.faq__collapse-row'),
          collapseArrow = $('.faq__collapse-arrow'),
          collapseInner = $('.faq__collapse-inner');


    collapseRow.on('click', function() {
        let $this = $(this);
        $this.find(collapseArrow).toggleClass('open');
        $this.siblings(collapseInner).slideToggle(400);
    });

});