<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
$this->setFrameMode(true);

$this->addExternalCss('/local/static/build/css/components/faq.css');
?>

<div class="company-video">
    <video src="/local/templates/aspro_optimus/images/Matras.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"' controls></video>
</div>

<section class="faq">
    <div class="faq__wrap">
        <h2 class="faq__title"><?=$arResult["IBLOCK_ELEMENT_IBLOCK_NAME"];?></h2>
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <div class="faq__collapse">
                <div class="faq__collapse-row">
                    <span class="faq__collapse-title"><?=$arItem["NAME"];?></span>
                    <div class="faq__collapse-arrow"></div>
                </div>
                <div class="faq__collapse-inner">
                    <?=$arItem["PREVIEW_TEXT"];?>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</section>
